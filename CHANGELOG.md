## [1.3.19 ~ 1.3.20]
- add keywords in intelissense
- add LICENSE file

## [1.3.17 ~ 1.3.18]
- fix duplicate object on internal model
- fix indexes of temp-tables when using tag's e.g. 'serialize-name'
- add dataset support on completion and definition provider
- fix bug when using live share


## [1.3.12 ~ 1.3.16]
- Now key field's come first on autocomplete
- Fix bug to show field's of a temp-table like on autocomplete
- Compiler now creates folders on deploy path
- Hover now show buffer's, temp-table's, and format for field's
- Check-syntax and Compiler show's how program were affected
- Fix bug's on temp-table's map
- Read include's recursively
- Fix bug on compiler messages

## [1.3.11]
- Breadcumbs now works!

## [1.3.5]
- A lot of small changes and bug's fixed

## [1.3.0]
- Fix error that plugin does not show error's on compiling option
- Added support to plugin TOTVS Healthcare Dev

## [1.2.0]
- Initial release