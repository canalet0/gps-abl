# Progress 4GL Abl Script #

A simple Progress 4GL Abl Script 4GL language extension based on incredible 3P plugin for Notepad++ from jcaillon (https://jcaillon.github.io/3P/).
To use all features of this extension see the usage chapter on this README.

## Repository
 - https://bitbucket.org/canalet0/gps-abl/
 
## Issues
 - https://bitbucket.org/canalet0/gps-abl/issues

## Features:
* A powerful Intellisense for Progress 4GL Abl Script 4GL that's support:
    - Include's informations
    - Temp-table fields
    - Database tables and fields
    - Procedure's scoped variables

* Intellisense also help's you with:
    - Database table's and temp-table indexes (show and auto-inject fields)
    - Procedure parameters

* Hovers / Tool tip's on code:
    - Shows data-type of variables and fields

* Code Navigator / Go to Symbol:
    - Temp-table definition
    - Variable definition
    - Procedures definition and call
    - Functions definition and call

* Check-syntax (command abl.checkSyntax):
    - With Shift + F2 you can test and visualize syntax error's

* Compilation (command abl.compile):
    - With Shift + F1 you can compile and deploy your programs

* Database information:
    - With command abl.dumpDatabaseStructure you can dump your database structure to use them for intellisense

## Next's step's:
- Fix bug's
- Support for Progress Help
- Support for DataDigger
- Support for goto function call's

## Usage:
Create a local config file for your project named .openedge.json, with the following structure:

{
    "parameterFiles": [
        "database.pf" //the path of your .pf file
    ],
	"workingDirectory": "rootPath", //root path of your progress workpace
    "proPath": [
        "propath1",
        "propath2",
        ...
    ],
    "dlc": "dlc116", //path where progress was installed
    "proPathMode": "append", 
    "databases" : ["srmovfin","srcadger"], //databases that you want to dump for intellisense
    "deploymentDirectory" : "C:/ro-115X" //path where .r will be deployed     
}

## Greetings
* OpenEdge ABL language from Camicas Christophe

