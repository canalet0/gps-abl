import { AblModel } from './models/ablModel';
import { Table, TableField, Variable, Method, BufferTable, AblObject } from './models/ablObject';
import * as vscode from 'vscode';
import { isNullOrUndefined } from 'util';

let databaseCompletionItens:vscode.CompletionItem[] = new Array<vscode.CompletionItem>();

export class CompletionItemAux extends vscode.CompletionItem {
    parseJsonToObject(param){
        Object.assign(this,param);                                        
    }
}

export class AblCompletionItemProvider implements vscode.CompletionItemProvider {

    public ablModel:AblModel;

    constructor(ablModel){
        this.ablModel = ablModel;
    }

    public provideCompletionItems(document: vscode.TextDocument, 
                                  position: vscode.Position, 
                                  token: vscode.CancellationToken): vscode.CompletionItem[] {
        return this.provideCompletionItemsInternal(document, position, token, vscode.workspace.getConfiguration('abl', document.uri));
	}

    public provideCompletionItemsInternal(document: vscode.TextDocument, 
                                          position: vscode.Position, 
                                          token: vscode.CancellationToken, 
                                          config: vscode.WorkspaceConfiguration): vscode.CompletionItem[] {

        let completionItens:vscode.CompletionItem[];

        let currentWord = "";
        let previousWord = "";
        let lineText = document.lineAt(position.line).text;
        let wordAtPosition = document.getWordRangeAtPosition(position);

        try {
            if (!isNullOrUndefined(wordAtPosition) 
            && wordAtPosition 
            && wordAtPosition.start.character < position.character) {
                let word = document.getText(wordAtPosition);
                currentWord = word.substr(0, position.character - wordAtPosition.start.character);
            }

            try {
                if (!isNullOrUndefined(wordAtPosition)
                && wordAtPosition.start.character > 0){
                    let previousWordPosition:vscode.Position = new vscode.Position(position.line,wordAtPosition.start.character - 1);
                    let previousWordAtPosition = document.getWordRangeAtPosition(previousWordPosition);

                    if(previousWordAtPosition){
                        previousWord = document.getText(previousWordAtPosition);
                    }
                }
            } catch (error) {
                console.log("provideCompletionItemsInternal1",error);
            }
        } catch (error) {
            console.log("provideCompletionItemsInternal2",error);
        }

        if(lineText.charAt(position.character - 1) === "."){
            position = new vscode.Position(position.line,position.character - 1);
            previousWord = document.getText(document.getWordRangeAtPosition(position));
        }

        try {
            completionItens = this.getCompletionItens(document,currentWord,previousWord,position);
        } catch (error) {
            console.log("provideCompletionItemsInternal3",error);
        }

        return completionItens;
    }

    private getVariablesCompletionItens(document): vscode.CompletionItem[] {
        let variableCompletionItens:vscode.CompletionItem[] = new Array<vscode.CompletionItem>();
        let ablVariables:Variable[] = this.ablModel.getAblVariables(document);

        ablVariables.forEach(variable => {
            variableCompletionItens.push(variable.toCompletionItem());
        });
        
        return variableCompletionItens;
    }

    private getMethodsCompletionItens(document,position: vscode.Position): vscode.CompletionItem[] {
        let methodCompletionItens:vscode.CompletionItem[] = new Array<vscode.CompletionItem>();
        let ablMethods:Method[] = this.ablModel.getAblMethodsObjects(document);

        ablMethods.forEach(method => {
            methodCompletionItens.push(method.toCompletionItem());

            if(document.fileName === method.programName
            && position.line >= method.init
            && position.line <= method.end){
                methodCompletionItens = methodCompletionItens.concat(method.getAllMethodVariables());
            }
        });

        return methodCompletionItens;
    }
        
    private getCompletionItens(document,currentWord:string,previousWord:string,position: vscode.Position): vscode.CompletionItem[] {    
        let objectCompletionItens:Array<vscode.CompletionItem> = new Array<vscode.CompletionItem>();
        let fieldsCompletionItens:vscode.CompletionItem[] = new Array<vscode.CompletionItem>();
        let ablObjectsMap:Map<string,AblObject> = new Map();
        let willReturnDatabaseTables:Boolean = true;

        if((previousWord == "end" || currentWord == "end")
        || (previousWord == "procedure" || currentWord == "procedure")
        || (previousWord == "function" || currentWord == "function"))
            return;

        ablObjectsMap = this.ablModel.getAblTableObjectsMap(document,willReturnDatabaseTables);
        
        if(ablObjectsMap.has(previousWord.toLowerCase())){
            let result = this.getFieldsCompletionItens(previousWord,ablObjectsMap);
            return result;
        }

        objectCompletionItens = objectCompletionItens.concat(this.getMethodsCompletionItens(document,position));

        objectCompletionItens = objectCompletionItens.concat(this.getVariablesCompletionItens(document));

        objectCompletionItens = objectCompletionItens.concat(this.ablModel.keyWords);

        ablObjectsMap.forEach(item => {
            let table:any = item;

            if(!(table instanceof Table)
            && !(table instanceof BufferTable)){
                table = new Table();
                (table as Table).parseJsonToObject(item);
            }

            if(!(table instanceof BufferTable)
            || !table.isUnderMethod){
                objectCompletionItens.push(table.toCompletionItem());
            }
        });

        return objectCompletionItens;
    }

    private getFieldsCompletionItens(objectKey,ablObjectsMap:Map<string,AblObject>): vscode.CompletionItem[] {    
        let fieldsCompletionItens:vscode.CompletionItem[] = new Array<vscode.CompletionItem>();
        let object:Object = ablObjectsMap.get(objectKey);  
        let table:Table = null;
        let isBuffer:Boolean = false;

        if(isNullOrUndefined(object)){
            object = ablObjectsMap.get(objectKey.toLowerCase());  

            if(isNullOrUndefined(object))
                return fieldsCompletionItens;
        }

        if(object instanceof Table){
            table = object as Table;
        }else{
            table = ablObjectsMap.get((object as BufferTable).bufferTable.toString()) as Table;

            if(isNullOrUndefined(table))
                table = ablObjectsMap.get((object as BufferTable).bufferTable.toString().toLowerCase()) as Table;

            isBuffer = true;
        }

        if(isNullOrUndefined(table))
            return;

        if(table.isTempTable && table.tempLike != ""){
            table = this.concatLikeObjects(table,table.tempLike,ablObjectsMap);
        }

        table.fields.forEach(field => {
            fieldsCompletionItens.push(field.toCompletionItem());
        });

        try {
            if(!isNullOrUndefined(table.indexes)){
                table.indexes.forEach(index => {
                    let completionItem = index.toCompletionItem(table);

                    if (!isNullOrUndefined(completionItem))
                        fieldsCompletionItens.push(completionItem);
                });
            }
        } catch (error) {
            console.log("getFieldsCompletionItens",error);
        }

        fieldsCompletionItens.push(table.allFieldsCompletionItem(isBuffer ? (object as BufferTable).name : ""));

        if(!table.isTempTable){
            fieldsCompletionItens.push(table.toTempTable());
            fieldsCompletionItens.push(table.toTempTableCC());
        }

        return fieldsCompletionItens;
    }

    private concatLikeObjects(table:Table,tempLike:string,ablObjectsMap:Map<string,AblObject>):Table{
        let likeTable:Table = ablObjectsMap.get(tempLike) as Table;  
        let concatTable:Table = new Table();

        concatTable = Object.assign(concatTable, table);

        if(isNullOrUndefined(likeTable)){
            likeTable = ablObjectsMap.get(tempLike.toLowerCase()) as Table;  
            if(isNullOrUndefined(likeTable))
                return;
        }

        concatTable.fields = concatTable.fields.concat(likeTable.fields);

        if(likeTable.isTempTable && likeTable.tempLike != ""){
            concatTable = this.concatLikeObjects(concatTable,likeTable.tempLike,ablObjectsMap);
        }

        return concatTable;
    }

}
