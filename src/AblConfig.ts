import { resolve } from 'url';
import { DatabaseConfig, AblModel } from './models/ablModel';
import { FileSystemWatcher, workspace } from 'vscode';
import { promisify, isNull, isNullOrUndefined } from 'util';
import { readFile } from 'fs';
import * as jsonminify from 'jsonminify';
import * as vscode from 'vscode';
import { configure } from 'vscode/lib/testrunner';
import { Table } from './models/ablObject';
import path = require('path');

let dataBaseConfig: DatabaseConfig;
let watcher: FileSystemWatcher;
let openEdgeConfig: OpenEdgeConfig = null;

const readFileAsync = promisify(readFile);
export const DATABASE_CONFIG_FILENAME = '.databaseConfig.json';
export const OPENEDGE_CONFIG_FILENAME = '.openedge.json';

export interface OpenEdgeConfig {
    dlc?: string;
    proPath?: string[];
    proPathMode?: 'append' | 'overwrite' | 'prepend';
    parameterFiles?: string[];
    workingDirectory?: string;
    databases?: string[];
    deploymentDirectory?: string;
}

function findConfigFile() {
    return workspace.findFiles(OPENEDGE_CONFIG_FILENAME).then(uris => {
        if (uris.length > 0) {
            return uris[0].fsPath;
        }
        return null;
    });
}

function loadAndSetConfigFile(filename: string) {
    if (filename === null) {
        return Promise.resolve({});
    }
    return loadConfigFile(filename).then((config) => {
        dataBaseConfig = config;
        return dataBaseConfig;
    });
}

export async function loadConfigFile(filename: string): Promise<any> {

    return new Promise<any | null>((resolve, reject) => {
        if (!filename)
            reject(null);

        resolve(
            readFileAsync(filename, { encoding: 'utf8' }).then(text => {
                // We don't catch the parsing error, to send the error in the UI (via promise rejection)
            return JSON.parse(jsonminify(text));
            })
        );
    });
}

export function getOpenEdgeConfig() {
    return new Promise<OpenEdgeConfig | null>((resolve, reject) => {
        if (openEdgeConfig === null) {
            watcher = workspace.createFileSystemWatcher('**/' + OPENEDGE_CONFIG_FILENAME);
            watcher.onDidChange(uri => loadAndSetConfigFile(uri.fsPath));
            watcher.onDidCreate(uri => loadAndSetConfigFile(uri.fsPath));
            watcher.onDidDelete(uri => loadAndSetConfigFile(uri.fsPath));

            findConfigFile().then(filename => loadAndSetConfigFile(filename)).then(config => resolve(config));
        } else {
            resolve(openEdgeConfig);
        }
    });
}

export function getOpenEdgeKeywords():vscode.CompletionItem[]{

    const keywordsFileDirectory = path.join(__dirname, '../abl-src/keywords/');
    const fs = require('fs');
    let objects:vscode.CompletionItem[] = new Array();

    fs.readdir(keywordsFileDirectory, (err, files) => {
        files.forEach(file => {
            return new Promise<any | null>((resolve, reject) => {                    
                resolve(
                    readFileAsync(keywordsFileDirectory + file, { encoding: 'utf8' }).then(text => {
                        // We don't catch the parsing error, to send the error in the UI (via promise rejection)
                        text.split(';').forEach(element => {
                            if(element === element.toUpperCase())
                                element = element.toLowerCase();
                            objects.push(new vscode.CompletionItem(element,vscode.CompletionItemKind.Keyword));
                        });
                    })
                );
            }); 
        });
    });

    return objects;
}

export function getDatabaseItens(filename):Promise<Table[]>{

    try {
        
    return new Promise<Table[] | null>((resolve, reject) => {
        let oboe = require('oboe');
        let fs = require('fs');
        oboe(fs.createReadStream(workspace.workspaceFolders[0].uri.fsPath + '/.' + filename))
        .node('tables.*', function( foodThing ){
         })
        .done(function(databaseTables) {
            resolve(databaseTables);
        })
        .fail(function() {
        });
    });

    } catch (error) {
        console.log("erro no parse json database",error)    ;
    }
}


