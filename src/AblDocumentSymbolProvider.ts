import * as vscode from 'vscode';
import { AblModel } from './models/ablModel';
import { Method, Variable, Table, AblObject, CallProcedure } from './models/ablObject';

export class AblDocumentSymbolProvider implements vscode.DocumentSymbolProvider {

    public ablModel:AblModel;

    constructor(ablModel){
        this.ablModel = ablModel;
    }

    public provideDocumentSymbols(document: vscode.TextDocument, 
                                  token: vscode.CancellationToken):vscode.SymbolInformation[] {

        let calls:CallProcedure[] = this.ablModel.getAblCallObjects(document,false);                                    
        let ablMethods:Method[] = this.ablModel.getAblMethodsObjects(document,false);
        let ablVariables:Variable[] = this.ablModel.getAblVariables(document,false);
        let ablTables:Map<string,any>  = this.ablModel.getAblTableObjectsMap(document,false,true);
        let documentSymbol:vscode.SymbolInformation[] = new Array();

        //return program item
        documentSymbol.push(new vscode.SymbolInformation(
            document.fileName,
            vscode.SymbolKind.Class,
            '',
            new vscode.Location(document.uri,new vscode.Position(0,0))
        ));

        calls.forEach(call => {
            documentSymbol.push(call.toDocumentSymbol());
        })

        ablMethods.forEach(method => {
            documentSymbol.push(method.toDocumentSymbol());
        });

        ablTables.forEach(table => {
            documentSymbol.push(table.toDocumentSymbol());
        });

        ablVariables.forEach(variable => {
            documentSymbol.push(variable.toDocumentSymbol());
        });

        return documentSymbol;
    }
}