import { AblModel } from './models/ablModel';
import { Table, TableField, Variable, Method } from './models/ablObject';
import * as vscode from 'vscode';
import { runInNewContext } from 'vm';

let databaseCompletionItens:vscode.CompletionItem[] = new Array<vscode.CompletionItem>();

export class AblDefinitionProvider implements vscode.DefinitionProvider {
    
    public ablModel:AblModel;

    constructor(ablModel){
        this.ablModel = ablModel;
    }

    public provideDefinition(document: vscode.TextDocument, 
                             position: vscode.Position, 
                             token: vscode.CancellationToken):vscode.Location {

        let wordAtPosition = document.getWordRangeAtPosition(position);
        let word = document.getText(wordAtPosition);

        let variables:Array<Variable> = this.ablModel.getAblVariables(document);
        let methods:Array<Method> = this.ablModel.getAblMethodsObjects(document);
        let tables:Map<string,any> = this.ablModel.getAblTableObjectsMap(document,false,false);
        let tableLocation:vscode.Location = null;

        for(var i = 0; i < variables.length; i ++){
            if(variables[i].name.toString() == word){
                return new vscode.Location(vscode.Uri.file(variables[i].programName.toString()),
                                           variables[i].definition);  
            }
        }

        for(var i = 0; i < methods.length; i ++){
            if(methods[i].name.toString() == word){
                return new vscode.Location(vscode.Uri.file(methods[i].programName.toString()),
                                           methods[i].definition);    
            }
        }

        /*tables.forEach(item => {
            if(item.name.toString() == word){
                tableLocation =  new vscode.Location(vscode.Uri.file(item.programName.toString()),
                                           item.definition);  
            }
        });*/

        if(tables.has(word.toLowerCase())){
            return new vscode.Location(vscode.Uri.file(tables.get(word.toLowerCase()).programName.toString()),
                                                       tables.get(word.toLowerCase()).definition);  
        }

        return tableLocation;
    }
}