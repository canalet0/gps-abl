import { AblModel } from './models/ablModel';
import { HoverProvider, Hover, MarkedString, TextDocument, Position, CancellationToken, WorkspaceConfiguration } from 'vscode';
import { Table, TableField, BufferTable, AblObject } from './models/ablObject';
import * as vscode from 'vscode';
import { isNullOrUndefined } from 'util';

export class AblHoverProvider implements HoverProvider {

    private ablModel:AblModel;

    public provideHover(document: TextDocument, 
                        position: Position, 
                        token: CancellationToken): Hover {
        return this.provideHoverInternal(document, position, token, vscode.workspace.getConfiguration('abl', document.uri));
    }

    public provideHoverInternal(document: vscode.TextDocument, 
        position: vscode.Position, 
        token: vscode.CancellationToken, 
        config: vscode.WorkspaceConfiguration): Hover {

        let currentWord = "";
        let previousWord = "";
        let lineText = document.lineAt(position.line).text;
        let wordAtPosition = document.getWordRangeAtPosition(position);

        try {
            if (wordAtPosition) {
                currentWord = document.getText(wordAtPosition);
            }

            try {
                let previousWordPosition:vscode.Position = new vscode.Position(position.line,wordAtPosition.start.character - 1);
                let previousWordAtPosition = document.getWordRangeAtPosition(previousWordPosition);

                if(previousWordAtPosition){
                    previousWord = document.getText(previousWordAtPosition);
                }
            } catch (error) {
            }
        } catch (error) {
        }

        if(lineText.charAt(position.character - 1) == "."){
            position = new vscode.Position(position.line,position.character - 1);
            previousWord = document.getText(document.getWordRangeAtPosition(position));
        }

        return this.getHover(document,currentWord,previousWord);
    }

    private getHover(document,currentWord,previousWord): Hover {

        let hover:vscode.Hover = null;
        let isField:Boolean = false;

        let ablObjects:Map<string,any> = this.ablModel.getAblTableObjectsMap(document);

        if(ablObjects.has(currentWord)){
            let object = ablObjects.get(currentWord) as AblObject;

            hover = new Hover("Table");

            if(!isNullOrUndefined(object) && object instanceof Table && object.isTempTable){
                hover = new Hover("Temp-table " + (object.tempLike != "" ? " like " + object.tempLike : ""));
            }

            if(!isNullOrUndefined(object) && object instanceof BufferTable)
                hover = new Hover("Buffer table for: " + object.bufferTable);
            
        }

        if(ablObjects.has(previousWord)
        || ablObjects.has(previousWord.toLowerCase())){
            let table:Table = ablObjects.get(previousWord);

            if(isNullOrUndefined(table))
                table = ablObjects.get(previousWord.toLowerCase());

            if(!isNullOrUndefined(table)
            && table.isTempTable 
            && table.tempLike != "")
                table = ablObjects.get(table.tempLike.toLowerCase());

            if(!isNullOrUndefined(table.fields)){
                for(let j = 0; j < table.fields.length;j++){
                    let field:TableField = table.fields[j];

                    if(field.fieldName.toString() == currentWord){
                        if(field.isLike)
                            hover = this.getHoverFromLikeObject(field,ablObjects);            
                        else hover = this.getHoverFromField(field);

                        break;
                    }
                }
            }
        }

        return hover;
    }

    getHoverFromLikeObject(field:TableField,ablObjects:Map<string,any>):Hover{
        let table:Table;
        let tableName:string = field.fieldType.substring(0,field.fieldType.lastIndexOf("."));
        let tableFieldName:string = field.fieldType.substring(field.fieldType.lastIndexOf(".") + 1);
        let hover:Hover = null;

        table = ablObjects.get(tableName);
        
        if(isNullOrUndefined(table))
            return this.getHoverFromField(field);

        if(isNullOrUndefined(table.fields))
        return this.getHoverFromField(field);

        for(let j = 0; j < table.fields.length;j++){
            let tableField:TableField = table.fields[j];
            if(tableField.fieldName.toString() == tableFieldName){
                hover = this.getHoverFromField(tableField);
                break;
            }
        }

        if(isNullOrUndefined(hover))
            hover = this.getHoverFromField(field);

        return hover;
    }

    getHoverFromField(field:TableField):Hover{
        let hoverText:string = "Type: "  + field.fieldType.toString();

        if(!isNullOrUndefined(field.fieldFormat)
        && field.fieldFormat.trim() != "")
            hoverText = hoverText + " Format: " + field.fieldFormat;

        return new Hover(hoverText);
    }
    
    constructor(ablModel){
        this.ablModel = ablModel;
    }
}
