import { TableField, Table } from './models/ablObject';
import { AblHoverProvider } from './ablHover';
import { AblCompletionItemProvider } from './AblCompletion';
import { AblDefinitionProvider } from './ablDefinition';
import { DocumentManager } from './shared/DocumentManager';
import { AblModel } from './models/ablModel';
import { FileSystemWatcher, workspace } from 'vscode';
import * as jsonminify from 'jsonminify';

'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { getDatabaseItens, getOpenEdgeConfig, getOpenEdgeKeywords } from './AblConfig';
import { resolve } from 'url';
import * as fs from 'fs';
import { AblDocumentSymbolProvider } from './AblDocumentSymbolProvider';
import { runBuilds } from './openEdge/ablCheckSyntax';
import { dumpDataBase } from './openEdge/ablDumpDatabse';
import { isNullOrUndefined } from 'util';

let ablModel:AblModel = new AblModel();
let documentManager:DocumentManager = new DocumentManager(ablModel);
let errorDiagnosticCollection: vscode.DiagnosticCollection;
let sucessDiagnosticCollection: vscode.DiagnosticCollection;
let warningDiagnosticCollection: vscode.DiagnosticCollection;

export const ABL_MODE: vscode.DocumentFilter = { language: 'abl', scheme: 'file' };

export async function activate(context: vscode.ExtensionContext) {    

    //Language Configuration
    context.subscriptions.push(vscode.languages.setLanguageConfiguration('abl',{wordPattern: /[\w'-]+/}));

    //Extension Providers
    context.subscriptions.push(vscode.languages.registerDefinitionProvider('abl', new AblDefinitionProvider(ablModel)));	
    context.subscriptions.push(vscode.languages.registerCompletionItemProvider('abl', new AblCompletionItemProvider(ablModel), '.', '\"'));
    context.subscriptions.push(vscode.languages.registerHoverProvider('abl', new AblHoverProvider(ablModel)));
    context.subscriptions.push(vscode.languages.registerDocumentSymbolProvider('abl',new AblDocumentSymbolProvider(ablModel)));

    //Extension Command's
    context.subscriptions.push(vscode.commands.registerCommand('abl.checkSyntax', () => {
        let ablConfig = vscode.workspace.getConfiguration('abl');
        runBuilds(vscode.window.activeTextEditor.document, ablConfig);
    }));

    context.subscriptions.push(vscode.commands.registerCommand('abl.compile', (teste?) => {
        return new Promise(function(resolve,reject) {
            let ablConfig = vscode.workspace.getConfiguration('abl');
            if(!isNullOrUndefined(teste)){
                vscode.workspace.openTextDocument(teste).then(doc => {
                    runBuilds(doc, ablConfig, true).then(result =>{
                        let file:string[] = teste.split("\\");
                        if(file.length == 0)
                            file = teste.split("/");
                        let finalFile:string = file[file.length - 1].replace(".p",".r");
                        fs.copyFile(
                            ablModel.configFile.deploymentDirectory + "/" + finalFile,
                            ablModel.configFile.workingDirectory + "/" + finalFile, 
                            (err) => {
                                if (err){
                                    reject();
                                }
                                console.log('source was copied to destination');
                                resolve();
                            }
                        );
                    });
                });
            }else resolve(runBuilds(vscode.window.activeTextEditor.document, ablConfig, true));
        });
    }));

    context.subscriptions.push(vscode.commands.registerCommand('abl.dumpDatabaseStructure', () => {
		let ablConfig = vscode.workspace.getConfiguration('abl');
		dumpDataBase(ablConfig);
    }));

    context.subscriptions.push(vscode.commands.registerCommand('abl.currentFile.getMap', () => {

		let doc = vscode.window.activeTextEditor.document;
		if (doc){
            try {
                let result = documentManager.getMap(vscode.window.activeTextEditor.document);
                return result;
            } catch (error) {
                console.log("error",error);
            }
        }else{
            return {};
        }
	}));
    
	/*context.subscriptions.push(vscode.commands.registerCommand('abl.dataDictionary', () => {
		// let ablConfig = vscode.workspace.getConfiguration('abl');
		openDataDictionary();
	}));*/

    //Text Document Observers
    context.subscriptions.push(vscode.workspace.onDidOpenTextDocument((document: vscode.TextDocument) => {
        documentManager.parseDocument(document);
    }));

    context.subscriptions.push(vscode.workspace.onDidSaveTextDocument((document: vscode.TextDocument) => {
        if(ablModel.hasProgram(document.fileName)){
            ablModel.deleteProgram(document);
        }

        documentManager.parseDocument(document);
    }));

    context.subscriptions.push(vscode.workspace.onDidCloseTextDocument((document: vscode.TextDocument) => {
        if(!ablModel.hasProgramRelation(document.fileName)){
            ablModel.deleteProgram(document);
        }
    }));

    //Document Parser for opened document's on startup
    for (let i = 0; i < vscode.workspace.textDocuments.length; ++i) {
        // Parse all words in this document
        documentManager.parseDocument(vscode.workspace.textDocuments[i]);
    }

    //Setup information box
    errorDiagnosticCollection = vscode.languages.createDiagnosticCollection('abl-error');
	context.subscriptions.push(errorDiagnosticCollection);
	warningDiagnosticCollection = vscode.languages.createDiagnosticCollection('abl-warning');
	context.subscriptions.push(warningDiagnosticCollection);
    
    //load configuration File
    getConfigFiles(ablModel);
}

export function clearDiagnostic(){
    if(!isNullOrUndefined(errorDiagnosticCollection))
        errorDiagnosticCollection.clear();
    
    if(!isNullOrUndefined(warningDiagnosticCollection))
        warningDiagnosticCollection.clear();

    if(!isNullOrUndefined(sucessDiagnosticCollection))
        sucessDiagnosticCollection.clear();
}

export function sucessPopUp(){
    return sucessDiagnosticCollection;
}

export function errorPopUp(){
    return errorDiagnosticCollection;
}

export function warningPopUp(){
    return warningDiagnosticCollection;
}

// this method is called when your extension is deactivated
export function deactivate() {
}

export async function getConfigFiles(ablModel:AblModel):Promise<any>{

    getOpenEdgeConfig().then(config => {
        vscode.window.showInformationMessage("Configurantion file sucessfully loaded!");
        ablModel.configFile = config;
        for(let i = 0; i < config.databases.length; i++){
            getDatabaseItens(config.databases[i])
            .then(tables =>{
                ablModel.addDatabaseTables(config.databases[i],tables);
            });
        }
    });
    ablModel.keyWords = getOpenEdgeKeywords();
}

