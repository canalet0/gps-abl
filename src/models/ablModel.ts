import { DocumentManager } from './../shared/DocumentManager';
import * as vscode from 'vscode';
import { Table,Program,Variable,Method, Include, BufferTable, AblObject, CallProcedure } from "./ablObject";
import { OpenEdgeConfig } from '../AblConfig';
import { isNullOrUndefined } from 'util';

export class AblModel {
    public openEdgeDatabaseTables:Table[];
    public openEdgeDatabaseTablesMap:Map<string,Table>;
    public programs:Program[];
    public lastMethod:Method;
    public configFile:OpenEdgeConfig;
    public keyWords:vscode.CompletionItem[];
    
    constructor(){
        this.openEdgeDatabaseTables = new Array<Table>();
        this.openEdgeDatabaseTablesMap = new Map();
        this.programs = new Array<Program>();
    }

    public getAblTableObjectsMap(document,getDatabaseTables:Boolean = true,onlyProgram:Boolean = false):Map<string,AblObject>{

        let ablObjects:Map<string,any> = new Map();
        let currentProgram:Program = this.getProgram(document);

        if(getDatabaseTables
        && !onlyProgram){
            ablObjects = this.openEdgeDatabaseTablesMap;
        }

        if(currentProgram != null){
            if(currentProgram.tablesMap != null
            && currentProgram.tablesMap.size > 0){
                currentProgram.tablesMap.forEach(function(value, key) {
                    ablObjects.set((key as string).toLowerCase(), value);
                });
            }

            if(!onlyProgram){
                currentProgram.includes.forEach(include => {
                    if(include.program.tablesMap != null
                    && include.program.tablesMap.size > 0){
                        
                        include.program.tablesMap.forEach(function(value, key) {
                            ablObjects.set((key as string).toLowerCase(), value);
                        });
                    }
                });
            }
        }

        return ablObjects;
    }

    public getAblTableObjects(document,onlyProgram:boolean = true,withInclude = false):Array<Table>{
        let ablObjects:Table[] = new Array<Table>();
        let currentProgram:Program = this.getProgram(document);

        if(!onlyProgram){
            withInclude = true;
            ablObjects = ablObjects.concat(this.openEdgeDatabaseTables);
        }
        
        if(currentProgram != null
        && currentProgram.tables != null
        && currentProgram.tables.length > 0){
            ablObjects = ablObjects.concat(currentProgram.tables);

            if(withInclude
            && currentProgram.includes !== null){
                currentProgram.includes.forEach(include => {
                    if(include.program.tables !== null){
                        ablObjects = ablObjects.concat(include.program.tables);
                    }
                });
            }
        }

        return ablObjects;
    }

    public getAblMethodsObjects(document,withInclude:boolean = true):Array<Method>{
        let ablMethods:Method[] = new Array<Method>();
        let currentProgram:Program = this.getProgram(document);

        if(currentProgram != null
        && currentProgram.methods != null
        && currentProgram.methods.length > 0){            
            ablMethods = ablMethods.concat(currentProgram.methods);

            if(withInclude
            && currentProgram.includes !== null){
                currentProgram.includes.forEach(include => {
                    if(include.program.methods !== null)
                        ablMethods = ablMethods.concat(include.program.methods);
                });
            }
        }

        return ablMethods;
    }

    public getAblCallObjects(document,withInclude:boolean = true):Array<CallProcedure>{
        let ablMethods:CallProcedure[] = new Array<CallProcedure>();
        let currentProgram:Program = this.getProgram(document);

        if(currentProgram != null
        && currentProgram.calls != null
        && currentProgram.calls.length > 0){            
            ablMethods = ablMethods.concat(currentProgram.calls);
        }

        return ablMethods;
    }

    public getAblVariablesMap(document,withInclude:boolean = true):Map<string,AblObject>{
        let ablVariables:Map<string,AblObject> = new Map();

        let currentProgram:Program = this.getProgram(document);

        if(currentProgram !== null
        && currentProgram.variablesMap !== null
        && currentProgram.variablesMap.size > 0){
            ablVariables = currentProgram.variablesMap;

            if(withInclude
            && currentProgram.includes !== null){
                currentProgram.includes.forEach(include => {
                    if(include.program.variablesMap !== null && include.program.variablesMap.size > 0){
                        include.program.variablesMap.forEach(function(value, key) {
                            ablVariables.set(key, value);
                        });
                    }
                });
            }
        }

        return ablVariables;
    }
    

    public getAblVariables(document,withInclude:boolean = true):Array<Variable>{
        let ablVariables:Variable[] = new Array<Variable>();
        let currentProgram:Program = this.getProgram(document);

        if(currentProgram !== null
        && currentProgram.variables !== null
        && currentProgram.variables.length > 0){
            ablVariables = ablVariables.concat(currentProgram.variables);

            if(withInclude
            && currentProgram.includes !== null){

                currentProgram.includes.forEach(include => {
                    if(include.program.variables !== null && include.program.variables.length > 0){
                        ablVariables = ablVariables.concat(include.program.variables);
                    }
                });
            }
        }

        return ablVariables;
    }

    public hasProgram(document){

        let documentFileName:string = "";

        if (typeof document !== "string")
            documentFileName = document.fileName;
        else documentFileName = document.toString();

        documentFileName = documentFileName.replace(new RegExp('/', 'g'),"\\");

        for(let i = 0; i < this.programs.length; i++){
            let realProgramName = this.programs[i].program.replace(new RegExp('/', 'g'),"\\");
            if(realProgramName == documentFileName){
                return true;
            }
        }

        return false;
    }

    public hasProgramRelation(document){

        let documentFileName = document;

        if (typeof document !== "string")
            documentFileName = document.fileName;

        if(!this.hasProgram(document))
            return false;

        for(let i = 0; i < this.programs.length; i++){
            for(let j = 0; j < this.programs[i].includes.length; j++){
                if(this.programs[i].includes[j] == documentFileName){
                    return true;
                }
            }
        }

        return false;
    }

    public deleteProgram(document){
        let documentFileName = document;

        if (typeof document !== "string")
            documentFileName = document.fileName;

        for(let i = 0; i < this.programs.length; i++){
            if(this.programs[i].program == documentFileName){
                this.programs.splice(i,1);
            }
        }
    }

    public clearModel(){
        this.programs = new Array<Program>();
    }

    public addVariable(document,variable:Variable){
        let program:Program = this.getProgram(document);

        variable.programName = program.program;

        if(program.lastMethod != null
        && variable.isParameter){
            program.lastMethod.parameters.push(variable);
            program.lastMethod.parametersMap.set(variable.getKey(),variable);

        }else{
            for(var i = 0; i < program.variables.length; i++){
                if(program.variables[i].name == variable.name)
                    return;
            }

            if(program.lastMethod != null){
                program.lastMethod.variables.push(variable);
                program.lastMethod.variablesMap.set(variable.getKey(),variable);
            }else{
                program.variables.push(variable);
            }
        }
    }

    public addDatabaseTables(database,tables:Table[]){
        this.openEdgeDatabaseTables = this.openEdgeDatabaseTables.concat(tables);
        tables.forEach(item => {
            let table = new Table();
            table.parseJsonToObject(item);
            this.openEdgeDatabaseTablesMap.set(table.getKey(),table);
        });
    }

    public addBufferTable(document,buffer:BufferTable){
        let program:Program = this.getProgram(document);
        
        buffer.programName = program.program;
        
        if(program.lastMethod != null){
            buffer.isUnderMethod = true;
            program.lastMethod.buffers.push(buffer);
        }

        program.buffers.push(buffer);
        program.tablesMap.set(buffer.getKey(),buffer);
    }

    public addCallProcedure(document,call:CallProcedure){
        let program:Program = this.getProgram(document);
        call.programName = program.program;

        program.calls.push(call);
    }

    public addTempTable(document,tempTable:Table){
        let program:Program = this.getProgram(document);
        
        tempTable.programName = program.program;

        if(isNullOrUndefined(tempTable)
        || isNullOrUndefined(tempTable.name))
            return;

        program.tables.push(tempTable);
        program.tablesMap.set(tempTable.getKey(),tempTable);
    }

    public addMethod(document,method:Method){

        let program:Program = this.getProgram(document);

        method.programName = program.program;
        program.methods.push(method);
        program.lastMethod = method;
    }

    public cleanMethod(){
        
        this.lastMethod = null;
    }

    public getProgram(document):Program{
        let program:Program = null;
        let documentFileName = document;

        if (typeof document !== "string"){
            documentFileName = document.fileName;
        }

        for(let i = 0; i < this.programs.length; i++){
            if(this.programs[i].program == documentFileName){
                program = this.programs[i];
                break;
            }
        }

        if(isNullOrUndefined(program)){
            program = new Program(documentFileName);
        }
            
        return program;
    }

    private getProgramIncludes(document):Program{

        let documentFileName = document;
        
        if (typeof document !== "string")
            documentFileName = document.fileName;

        for(let i = 0; i < this.programs.length; i++){
            if(this.programs[i].program = documentFileName){
                return this.programs[i];
            }
        }

        return null;
    }

    public addInclude(document:vscode.TextDocument,includeId:string,includeTag:string=''){

        let program:Program = this.getProgram(document);
        let include:Include = new Include();
        let hasInclude:Boolean = false;
        include.path = vscode.workspace.workspaceFolders[0].uri.fsPath + '/' + includeId;
        include.program = this.getProgram(includeId);
        include.tag = includeTag;

        if(isNullOrUndefined(include.program))
            return;

        program.includes.forEach(programInclude => {
            if(include.program.program == programInclude.program.program)
                hasInclude = true;
        });

        if(hasInclude)
            return;

        if(include.program == null){
            let programInclude:Program = new Program(includeId);
            include.program = programInclude;
        }

        program.includes.push(include);

    }

    public setMethodFinalLine(document:vscode.TextDocument,methodFinalLine:Number){

        let program:Program = this.getProgram(document);

        if(program.lastMethod != null){
            program.lastMethod.end = methodFinalLine;
        }

        program.lastMethod = null;
    }

    public updatePrograms(program:Program){
        if(program.program.trim().endsWith('.i')){
            this.programs.forEach(element => {
                element.includes.forEach(include => {
                    if(include.program.fileNameWithoutPath === program.fileNameWithoutPath){
                        include.program = program;
                    }
                });
            });
        }
    }

    public getMap(document,withInclude:boolean = true):Array<Method>{
        let ablMethods:Method[] = new Array<Method>();
        let currentProgram:Program = this.getProgram(document);

        if(currentProgram != null
        && currentProgram.methods != null
        && currentProgram.methods.length > 0){            
            ablMethods = ablMethods.concat(currentProgram.onlyProcedures());

            if(withInclude
            && currentProgram.includes !== null){
                currentProgram.includes.forEach(include => {
                    if(include.program.methods !== null)
                        ablMethods = ablMethods.concat(include.program.methods);
                });
            }
        }

        return ablMethods;
    }
}

export interface DatabaseConfig {
    databases?:string[];
}