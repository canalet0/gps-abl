
import { CompletionItemAux } from './../AblCompletion';
import * as vscode from 'vscode';
import { toObjectName, padRight } from '../shared/Utils';
import { isNullOrUndefined } from 'util';
import { removeComments} from '../shared/Utils';

export class AblObject {
    public programName:String;
    public name:String;
    public definition:vscode.Position;

    constructor(){
        this.programName = "";
        this.definition = null;
        this.name = "";
    }
}

export class BufferTable extends AblObject {
    public bufferId?:Number;
    public bufferTable:String;
    public isUnderMethod:Boolean;

    constructor(){
        super();
        this.bufferId = 0;
        this.bufferTable = "";
        this.isUnderMethod = false;
    }

    getKey(){
        return this.name.toString();
    }

    toCompletionItem():vscode.CompletionItem{

        let completionItem:vscode.CompletionItem = new vscode.CompletionItem(this.name.toString(),
                                                                            vscode.CompletionItemKind.Method);

        return completionItem;

    }

    toDocumentSymbol():vscode.SymbolInformation{
        return new vscode.SymbolInformation(this.name.toString(),
                                            vscode.SymbolKind.Method,
                                            '',
                                            new vscode.Location(vscode.Uri.file(this.programName.toString()),
                                                                this.definition));
    }
}

export class Table extends AblObject{
    public tableId?:Number;
    public fields?:TableField[];
    public indexes?:TableIndex[];
    public isTempTable:boolean;
    public tempLike:string;
    private fieldsMap?:Map<string,TableField>;
    
    constructor(){
        super();
        this.tableId = 0;
        this.isTempTable = false;
        this.fields = new Array<TableField>();
        this.indexes = new Array<TableIndex>();
        this.tempLike = "";
        this.fieldsMap = new Map();
    }

    getKey(){
        if(isNullOrUndefined(this.name))
            return "";

        return this.name.toString();
    }

    parseJsonToObject(param){
        Object.assign(this,param);

        let tableFields:TableField[] = new Array<TableField>();
        let tableIndexes:TableIndex[] = new Array<TableIndex>();

        this.fields.forEach(field => {
            let parsedField:TableField = new TableField();
            parsedField.parseJsonToObject(field);
            tableFields.push(parsedField);
            this.fieldsMap.set(field.fieldName.toString(),field);
        });
        this.fields = tableFields;

        this.indexes.forEach(index => {
            let parsedIndex:TableIndex = new TableIndex();
            parsedIndex.parseJsonToObject(index);
            tableIndexes.push(parsedIndex);
        });
        this.indexes = tableIndexes;
    }

    toCompletionItem():vscode.CompletionItem{

        let completionItem:vscode.CompletionItem = new vscode.CompletionItem(this.name.toString(),
                                                                            vscode.CompletionItemKind.Method);

        if(!this.isTempTable)
            completionItem.kind = vscode.CompletionItemKind.Class;

        return completionItem;

    }

    allFieldsCompletionItem(name:String = ""):vscode.CompletionItem{
        let allCompletionItem:vscode.CompletionItem = new vscode.CompletionItem("allFields",
                                                                                vscode.CompletionItemKind.Method);

        allCompletionItem.insertText = "";

        if(name == "")
            name = this.name;

        this.fields.forEach(field => {
            if(!field.isReserved
            && field.fieldName != undefined){

                if(allCompletionItem.insertText != "")
                    allCompletionItem.insertText += name + "." ;

                allCompletionItem.insertText += field.fieldName + "\n";
            }
        });

        return allCompletionItem;
    }

    toTempTableCC():vscode.CompletionItem{
        let toTempTable:vscode.CompletionItem = new vscode.CompletionItem("toTempTableCamelCase",
                                                                          vscode.CompletionItemKind.Method);

        return this.getCompletionTempTable(toTempTable,true);
    }

    toTempTable():vscode.CompletionItem{
        let toTempTable:vscode.CompletionItem = new vscode.CompletionItem("toTempTable",
                                                                          vscode.CompletionItemKind.Method);

        return this.getCompletionTempTable(toTempTable);
    }

    getCompletionTempTable(toTempTable:vscode.CompletionItem,
                          withCamelCase:Boolean = false,
                          withSerializedName = false):vscode.CompletionItem{

        toTempTable.insertText = "";

        let toTempTableInsertText:vscode.SnippetString = new vscode.SnippetString();

        toTempTable.insertText += 'define temp-table tmp-gerada no-undo ' + "\n";

        toTempTableInsertText.appendText("\n" + "define temp-table ");
        toTempTableInsertText.appendPlaceholder("tempTableName");
        toTempTableInsertText.appendText(" no-undo" + "\n");

        this.fields.forEach(field => {
            if(!field.isReserved
            && field.fieldName != undefined){

                toTempTableInsertText.appendText('\tfield ');

                if(withCamelCase)
                     toTempTableInsertText.appendText(padRight(toObjectName(field.fieldName),33));
                else toTempTableInsertText.appendText(padRight(field.fieldName.toString(),33));

                toTempTableInsertText.appendText(' like ' + this.name + '.' + field.fieldName + "\n");
            }
        });

        toTempTableInsertText.appendText(".");
        toTempTable.insertText = toTempTableInsertText;

        return toTempTable;
    }

    toDocumentSymbol():vscode.SymbolInformation{
        return new vscode.SymbolInformation(this.name.toString(),
                                            vscode.SymbolKind.Method,
                                            '',
                                            new vscode.Location(vscode.Uri.file(this.programName.toString()),
                                                                this.definition));
    }

    getField(field):TableField{
        if(!this.fieldsMap.has(field))
            return null;

        return this.fieldsMap.get(field);
    }
}

export class TableField {
    public tableId?     : Number;
    public fieldId?     : Number;
    public fieldName    : String;
    public fieldType    : String;
    public fieldLabel   : String;
    public fieldFormat  : String;
    public isKey?        : Boolean;
    public isReserved?   : Boolean;
    public additionalInformation?:string;
    public isLike       :Boolean;

    constructor(){
        this.tableId = 0;
        this.fieldId = 0;
        this.fieldName = "";
        this.isKey = false;
        this.isReserved = false;
        this.fieldType = "";
        this.fieldLabel = "";
        this.fieldFormat = "";
        this.additionalInformation = "";
        this.isLike = false;
    }

    parseJsonToObject(param){
        Object.assign(this,param);
    }

    toCompletionItem():vscode.CompletionItem{
        let completionItem:vscode.CompletionItem = new vscode.CompletionItem(this.fieldName.toString() + (this.isKey ? " (is key)" : ""),
                                                                             vscode.CompletionItemKind.Struct);

        completionItem.sortText = "1";

        if(this.isKey)
            completionItem.sortText = "0";
        if(this.isReserved)
            completionItem.sortText = "100";
        
        completionItem.insertText = this.fieldName.toString();

        let detail : string;

        if(this.isKey){
            completionItem.kind = vscode.CompletionItemKind.Field;
        }

        if(this.isReserved){
            completionItem.kind = vscode.CompletionItemKind.Variable;
        }

        if(this.fieldType == 'index'){
            completionItem.kind = vscode.CompletionItemKind.Class;
            completionItem.insertText = this.additionalInformation;
        }

        detail = "Type: " + this.fieldType;
        
        if(!isNullOrUndefined(this.fieldFormat)
        && this.fieldFormat.trim().length > 0){
            detail = detail + "\n" + "Format: " + this.fieldFormat;
        }
    
        completionItem.detail = detail;

        return completionItem;
    }
}

export class TableIndex {
    public name:String;
    public isPrimary?:Boolean;
    public indexFields?:IndexField[];

    constructor(){
        this.name = "";
        this.isPrimary = false;
    }

    parseJsonToObject(param){
        Object.assign(this,param);
    }

    toCompletionItem(table:Table):vscode.CompletionItem{
        let completionItem:vscode.CompletionItem = new vscode.CompletionItem(this.name.toString() + (this.isPrimary ? " primary " : ""),
                                                                             vscode.CompletionItemKind.Property);

        if(isNullOrUndefined(this.indexFields)){
            return null;
        }

        completionItem.insertText = "";
        completionItem.sortText = "200"

        if(this.isPrimary)
            completionItem.sortText = "101";

        let detail:String = this.name + (this.isPrimary ? " (primary) " : "") + "\n";        

        
        this.indexFields.forEach(element => {            
            detail = detail + (element.descending ? " - " : " + ") + element.field;
            if(!isNullOrUndefined(table)){
                let field:TableField = table.getField(element.field);

                if(!isNullOrUndefined(field)){
                    detail = detail + " : " + field.fieldType; 
                }
            }

            if(completionItem.insertText != "")
                completionItem.insertText += table.name + "." ;

            completionItem.insertText += element.field + "\n";

            detail = detail + "\n";
        });

        completionItem.detail = detail.toString();

        return completionItem;
    }
}

export class IndexField{
    public field:String;
    public descending:Boolean;

    constructor(){
        this.field = "";
        this.descending = false;
    }

    parseJsonToObject(param){
        Object.assign(this,param);
    }
}

export class Program{
    public program:String;
    public variables:Variable[];
    public variablesMap:Map<string,AblObject>;
    public methods:Method[];
    public tables:Table[];
    public buffers:BufferTable[];
    public tablesMap:Map<string,Object>;
    public includes:Include[];
    public calls:CallProcedure[];

    private programLastMethod:Method;
    
    constructor(fileName){
        this.program = fileName;
        this.variables = new Array<Variable>();
        this.methods = new Array<Method>();
        this.tables = new Array<Table>();
        this.buffers = new Array<BufferTable>();
        this.includes = new Array<Include>();
        this.tablesMap = new Map();
        this.variablesMap = new Map();
        this.calls = new Array<CallProcedure>();
    }

    get fileNameWithoutPath(){
        return this.program.replace(new RegExp('/', 'g'),"\\");
    }

    get lastMethod(){
        return this.programLastMethod;
    }

    set lastMethod(method:Method){
        this.programLastMethod = method;
    }

    public onlyProcedures():Method[]{
        let onlyProcedures:Method[] = new Array<Method>();

        this.methods.forEach(method => {
            if(!method.isFunction)
                onlyProcedures.push(method);
        });

        return onlyProcedures;
    }
}

export class Include {
    public path;
    public program:Program;
    public tag:string;
}

export class Variable extends AblObject {
    public fieldType                : String;
    public isParameter              : Boolean;
    public isTable                  : Boolean;
    public direction                : String;
    public additionalInformation    : String;
    public scope                    : String;

    getKey(){
        return this.name.toString();
    }

    toCompletionItem():vscode.CompletionItem {
        let completionItem: vscode.CompletionItem = new vscode.CompletionItem(this.name.toString(),
                                                                              vscode.CompletionItemKind.Variable);

        if(!isNullOrUndefined(this.fieldType))
            completionItem.detail = this.fieldType.toString();

        if(this.isParameter)
            completionItem.kind = vscode.CompletionItemKind.Property;

        return completionItem;
    }

    toDocumentSymbol():vscode.SymbolInformation{
        return new vscode.SymbolInformation(this.name.toString(),
                                            vscode.SymbolKind.Variable,
                                            '',
                                            new vscode.Location(vscode.Uri.file(this.programName.toString()),
                                                                this.definition));
    }
}

export class Method extends AblObject {
    public parameters:Variable[];
    public variables:Variable[];
    public buffers:BufferTable[];
    public variablesMap:Map<string,Object>;
    public parametersMap:Map<string,Object>;
    public isFunction:boolean;
    public init:Number;
    public end:Number;

    constructor(){
        super();
        this.parameters = new Array<Variable>();
        this.variables = new Array<Variable>();
        this.buffers = new Array<BufferTable>();
        this.variablesMap = new Map();
        this.parametersMap = new Map();
        this.isFunction = false;
    }

    toCompletionItem():vscode.CompletionItem {
        let completionItem: vscode.CompletionItem = new vscode.CompletionItem(this.name.toString(),
                                                                              vscode.CompletionItemKind.Module);

        let parameterList:string = "";
        let parameterText:String = "";
        let hasParameters = this.parameters.length > 0 ? true : false;
        let parametersInsertText:vscode.SnippetString = new vscode.SnippetString();
        
        completionItem.insertText = this.name + ".";

        if(this.isFunction)
            completionItem.kind = vscode.CompletionItemKind.Event;

        if(hasParameters){
            completionItem.insertText = this.name.toString() + "(";
            parametersInsertText.appendText(this.name.toString() + "(");
            parameterList = 'parameters: \n';

            for(var i = 0;i < this.parameters.length;i++){
                let parameter:Variable = this.parameters[i];

                parameterText = parameter.direction + " " + (parameter.fieldType == "table" ? "table" : "") + " ";
                parametersInsertText.appendText(parameterText.toString());
                parameterText = parameterText.concat(parameter.name.toString());
                parametersInsertText.appendPlaceholder(parameter.name.toString());

                if(i < this.parameters.length - 1)
                    parametersInsertText.appendText(",\n\t");

                if(parameter.fieldType != "table")
                    parameterText = parameterText.concat(" : " + parameter.fieldType);

                parameterList = parameterList.concat(" - " + parameterText + "\n");

            }
            parametersInsertText.appendText(").");
            completionItem.insertText = parametersInsertText;
        }

        completionItem.detail = parameterList;
        
        return completionItem;
    }

    toDocumentSymbol():vscode.SymbolInformation{
        let rangePosition:vscode.Range = new vscode.Range(new vscode.Position(this.init.valueOf(),0),
                                        new vscode.Position(this.end.valueOf(),0),);

        return new vscode.SymbolInformation(this.name.toString(),
                                            (this.isFunction ? vscode.SymbolKind.Event : vscode.SymbolKind.Module),
                                            '',
                                            new vscode.Location(vscode.Uri.file(this.programName.toString()),
                                                                rangePosition));
    }

    getAllMethodVariables():vscode.CompletionItem[]{
        let methodCompletionItens:vscode.CompletionItem[] = new Array<vscode.CompletionItem>();

        this.variables.forEach(variable => {
            methodCompletionItens.push(variable.toCompletionItem());
        });

        this.parameters.forEach(parameter => {
            methodCompletionItens.push(parameter.toCompletionItem());
        });

        this.buffers.forEach(buffer => {
            methodCompletionItens.push(buffer.toCompletionItem());
        });

        return methodCompletionItens;
    }
}

export class CallProcedure extends AblObject{
    
    toDocumentSymbol():vscode.SymbolInformation{
        return new vscode.SymbolInformation(this.name.toString(),
                                            (vscode.SymbolKind.Method),
                                            '',
                                            new vscode.Location(vscode.Uri.file(this.programName.toString()),
                                                                this.definition));
    }
}

export class AblStatement {
    public statement:string;
    public lineInit:number;
    public lineEnd:number;

    constructor(statement,lineInit,lineEnd){
        this.statement = removeComments(statement);
        this.lineInit = lineInit;
        this.lineEnd = lineEnd;
    }
}

