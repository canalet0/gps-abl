import { CompletionItemKind, CompletionList } from "vscode";


export enum SYMBOL_TYPE {
    METHOD = 'Method',
    INCLUDE = 'Include File',
    LOCAL_VAR = 'Local Variable',
    GLOBAL_VAR = 'Global Variable',
    LOCAL_PARAM = 'Local Parameter',
    GLOBAL_PARAM = 'Global Parameter',
    TEMPTABLE = 'Temp-table'
}

export enum ABL_ASLIKE {
    AS = 'as',
    LIKE = 'like'
}

export enum ABL_PARAM_DIRECTION {
    IN = 'input',
    OUT = 'output',
    INOUT = 'input-output'
}

export class ABLVariable {
    name: string;
    asLike: ABL_ASLIKE;
    dataType: string;
    line: number;
}

export class ABLParameter extends ABLVariable {
    direction: ABL_PARAM_DIRECTION;
}

export class ABLMethod {
    name: string;
    lineAt: number;
    lineEnd: number;
    params: ABLParameter[];
}

export interface ABLFieldDefinition {
    label: string;
    kind: CompletionItemKind;
    detail: string;
    dataType: string;
    mandatory: boolean;
    format: string;
}

export interface ABLIndexDefinition {
    label: string;
    kind: CompletionItemKind;
    detail: string;
    fields: ABLFieldDefinition[];
    unique: boolean;
    primary: boolean;
}

export class ABLTableDefinition {
    filename: string;
    label: string;
    kind: CompletionItemKind;
    detail: string;
    pkList: string;
    fields: ABLVariable[];
    indexes: ABLIndexDefinition[];
    completionFields: CompletionList;
    completionIndexes: CompletionList;
    completionAdditional: CompletionList;
    completion: CompletionList;

    get allFields(): ABLVariable[] {
        return this.fields;
    }
}

export class ABLTempTable extends ABLTableDefinition {
    line: number;
    referenceTable: string;
    referenceFields: ABLVariable[];

    get allFields(): ABLVariable[] {
        if (this.referenceFields)
            return [...this.referenceFields,...this.fields];
        return this.fields;
    }
}

export class ABLInclude {
    name: string;
    fsPath: string;
    map;
}