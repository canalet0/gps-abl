import * as vscode from 'vscode';
import cp = require('child_process');
import path = require('path');

//import { getProBin, createProArgs, setupEnvironmentVariables } from './shared/ablPath';
import { outputChannel } from '../ablStatus';
import { getOpenEdgeConfig } from '../AblConfig';
import { getProBin, setupEnvironmentVariables, createProArgs } from '../shared/ablPath';
import { errorPopUp, warningPopUp, clearDiagnostic } from '../extension';
import { isNullOrUndefined } from 'util';

let statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left);


export function removeTestStatus(e: vscode.TextDocumentChangeEvent) {
	if (e.document.isUntitled) { 
		return;
	}
	statusBarItem.hide();
	statusBarItem.text = '';
}

export interface ICheckResult {
	file: string;
	line: number;
	column: number;
	msg: string;
	severity: string;
}

export function checkSyntax(filename: string, ablConfig: vscode.WorkspaceConfiguration,compiler:Boolean = false): Promise<ICheckResult[]> {

	let source = '../../abl-src/check-syntax.p';
	outputChannel.clear();
	statusBarItem.show();

	if(!compiler){
		statusBarItem.text = 'Checking syntax';
	}else{
		statusBarItem.text = 'Compiling';
		source = '../../abl-src/compiler.p';
	} 

	vscode.window.showInformationMessage(statusBarItem.text);

	let cwd = path.dirname(filename);

	return getOpenEdgeConfig().then(oeConfig => {

		let cmd = getProBin(oeConfig.dlc);
		let env = setupEnvironmentVariables(process.env, oeConfig, vscode.workspace.workspaceFolders[0].uri.fsPath);
		let args = createProArgs({
			parameterFiles: oeConfig.parameterFiles,
			batchMode: true,
			startupProcedure: path.join(__dirname, source),
			param: filename,
			workspaceRoot: vscode.workspace.workspaceFolders[0].uri.fsPath
		});

		cwd = oeConfig.workingDirectory ? oeConfig.workingDirectory.replace('${workspaceRoot}', vscode.workspace.workspaceFolders[0].uri.fsPath).replace('${workspaceFolder}', vscode.workspace.workspaceFolders[0].uri.fsPath) : cwd;

		return new Promise<ICheckResult[]>((resolve, reject) => {
			cp.execFile(cmd, args, { env: env, cwd: cwd }, (err, stdout, stderr) => {
				try {
					if (err && (<any>err).code === 'ENOENT') {
						// Since the tool is run on save which can be frequent
						// we avoid sending explicit notification if tool is missing
						return resolve([]);
					}
					let useStdErr = false; // todo voir si utile
					if (err && stderr && !useStdErr) {
						outputChannel.appendLine(['Error while running tool:', cmd, ...args].join(' '));
						outputChannel.appendLine(stderr);
						return resolve([]);
					}
					let lines = stdout.toString().split('\r\n').filter(line => line.length > 0);
					if (lines.length === 1 && lines[0].startsWith('SUCCESS')) {
						resolve([]);
						return;
					}
					let results: ICheckResult[] = [];

					// Format = &1 File:'&2' Row:&3 Col:&4 Error:&5 Message:&6
					let re = /(ERROR|WARNING) File:'(.*)' Row:(\d+) Col:(\d+) Error:(.*) Message:(.*)/;
					lines.forEach(line => {
						let matches = line.match(re);

						if (matches) {
							let checkResult = {
								file: matches[2],
								line: parseInt(matches[3]),
								column: parseInt(matches[4]),
								msg: `${matches[5]}: ${matches[6]}`,
								severity: matches[1].toLowerCase()
							};
							results.push(checkResult);
						} else {
							reject(stdout);
						}
					});
					resolve(results);
				} catch (e) {
					reject(e);
				}
			});
		}).then(results => {
			/*cmd = null;
			env = null;
			args = null;*/

			if (results.length === 0){
				statusBarItem.text = compiler ? 'Compilation Completed! ' : 'Syntax is Correct! ';
				statusBarItem.text = statusBarItem.text + " Program: ";
				vscode.window.showInformationMessage(statusBarItem.text + " " + filename.substring(filename.lastIndexOf("\\") + 1)); 
			}else
				statusBarItem.text = 'Syntax error';
			return results;
		});
	});

}

export function runBuilds(document: vscode.TextDocument, ablConfig: vscode.WorkspaceConfiguration,compiler:Boolean = false) : Promise<any> {

	return new Promise(function(resolve,reject) {
		function mapSeverityToVSCodeSeverity(sev: string) {
			switch (sev) {
				case 'error': return vscode.DiagnosticSeverity.Error;
				case 'warning': return vscode.DiagnosticSeverity.Warning;
				default: return vscode.DiagnosticSeverity.Error;
			}
		}

		clearDiagnostic();

		if (document.languageId !== 'abl') {
			reject();
		}

		let uri = document.uri;
		checkSyntax(uri.fsPath, ablConfig,compiler).then(errors => {
			//errorDiagnosticCollection.clear();
			//warningDiagnosticCollection.clear();

			if(isNullOrUndefined(errors)
			|| errors.length == 0){
				resolve();
			}

			let diagnosticMap: Map<string, Map<vscode.DiagnosticSeverity, vscode.Diagnostic[]>> = new Map();

			errors.forEach(error => {
				let canonicalFile = vscode.Uri.file(error.file).toString();
				let startColumn = 0;
				let endColumn = 1;
				if (error.line === 0) {
					vscode.window.showErrorMessage(error.msg);
				}
				else {
					if (document && document.uri.toString() === canonicalFile) {
						let range = new vscode.Range(error.line - 1, startColumn, error.line - 1, document.lineAt(error.line - 1).range.end.character + 1);
						let text = document.getText(range);
						let [_, leading, trailing] = /^(\s*).*(\s*)$/.exec(text);
						startColumn = startColumn + leading.length;
						endColumn = text.length - trailing.length;
					}
					let range = new vscode.Range(error.line - 1, startColumn, error.line - 1, endColumn);
					let severity = mapSeverityToVSCodeSeverity(error.severity);
					let diagnostic = new vscode.Diagnostic(range, error.msg, severity);
					let diagnostics = diagnosticMap.get(canonicalFile);
					if (!diagnostics) {
						diagnostics = new Map<vscode.DiagnosticSeverity, vscode.Diagnostic[]>();
					}
					if (!diagnostics[severity]) {
						diagnostics[severity] = [];
					}
					diagnostics[severity].push(diagnostic);
					diagnosticMap.set(canonicalFile, diagnostics);
					
				}
			});
			diagnosticMap.forEach((diagMap, file) => {
				errorPopUp().set(vscode.Uri.parse(file), diagMap[vscode.DiagnosticSeverity.Error]);
				warningPopUp().set(vscode.Uri.parse(file), diagMap[vscode.DiagnosticSeverity.Warning]);

				try {
					diagMap[vscode.DiagnosticSeverity.Error].forEach(error => {
						vscode.window.showErrorMessage(error.message,error.range);				
					});
				} catch (error) {
						
				}

				try {
					diagMap[vscode.DiagnosticSeverity.Warning].forEach(error => {
						vscode.window.showWarningMessage(error.message,error.range);				
					});
				} catch (error) {
						
				}
			});
		}).catch(err => {
			vscode.window.showInformationMessage('Error: ' + err);
		});
	});
}
