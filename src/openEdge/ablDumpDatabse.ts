import * as vscode from 'vscode';
import cp = require('child_process');
import path = require('path');

//import { getProBin, createProArgs, setupEnvironmentVariables } from './shared/ablPath';
import { outputChannel } from '../ablStatus';
import { getOpenEdgeConfig } from '../AblConfig';
import { getProBin, setupEnvironmentVariables, createProArgs } from '../shared/ablPath';
import { errorPopUp, warningPopUp } from '../extension';

let statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left);

export function dumpDataBase(ablConfig: vscode.WorkspaceConfiguration): Promise<any> {
	outputChannel.clear();
	statusBarItem.show();
	statusBarItem.text = 'Extracting Database';

	vscode.window.showInformationMessage("Extracting Database Information");

	return getOpenEdgeConfig().then(oeConfig => {

		let cmd = getProBin(oeConfig.dlc);
		let env = setupEnvironmentVariables(process.env, oeConfig, vscode.workspace.workspaceFolders[0].uri.fsPath);
		let args = createProArgs({
			parameterFiles: oeConfig.parameterFiles,
			batchMode: true,
			startupProcedure: path.join(__dirname, '../../abl-src/dumpDatabaseInfo.p'),
			param: vscode.workspace.workspaceFolders[0].uri.fsPath,
			workspaceRoot: vscode.workspace.workspaceFolders[0].uri.fsPath
		});

		return new Promise<any>((resolve, reject) => {
			cp.execFile(cmd, args, { env: env }, (err, stdout, stderr) => {
				try {		
					
					resolve();
				} catch (e) {
					reject(e);
				}
			});
		}).then(results => {
			statusBarItem.text = 'Database Extraction Complete!';
			vscode.window.showInformationMessage("Database Extraction Complete!");
			
			if (results.length === 0){
				statusBarItem.text = 'Database Extraction Complete!';
				vscode.window.showInformationMessage("Database Extraction Complete!");
			}else{
				statusBarItem.text = 'Database Extraction Error!';
				vscode.window.showErrorMessage('Database Extraction Error!');
			}
			return results;
		});
	});
}
