import { Method, BufferTable, CallProcedure, TableIndex, IndexField } from './../models/ablObject';
/**
 * COPYRIGHT 2017 Atishay Jain<contact@atishay.me>
 *
 * MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
'use strict';
import * as vscode from 'vscode';
import * as path from 'path';
import fs = require('fs');
import { shouldExcludeFile, relativePath, toGPSMethod, toGPSVariables, toGPSTempTables, removeComments } from './Utils';
import { Program } from '../models/ablObject';
import { TableField } from './../models/ablObject';
import { AblStatement, Table, Variable } from './../models/ablObject';
import { AblModel } from './../models/ablModel';
import { ABLInclude } from '../models/gpsObjects';
import { isNullOrUndefined } from 'util';

/**
 * Class to manage addition and removal of documents from the index
 *
 * @class DocumentManagerClass
*/
export class DocumentManager {
    private paths: Map<string, string> = new Map<string, string>();
    private files: Map<string, string[]> = new Map<string, string[]>();
    private ablModel:AblModel;
    private functions:String[] = new Array();

    constructor(ablModel){
        this.ablModel = ablModel;
    }

    /**
     * Method to initialize the document manager.
     *
     * @memberof DocumentManagerClass
    */
    init() {

    }
    /**
     * Parses a document to create a trie for the document.
     *
     * @param {TextDocument} document
     * @memberof DocumentManagerClass
     */
    parseDocument(document: vscode.TextDocument) {

        let isUnderComment = false;
        
        if(document.languageId !== "abl"){
            return;
        }

        let program:Program = new Program(document.fileName.replace(new RegExp('/', 'g'),"\\"));

        if (shouldExcludeFile(document.fileName)) {
            return;
        }

        // Don't parse a document already present. The existing document
        // case takes place when
        if (this.ablModel.hasProgram(document)) {
            return;
        }

        this.ablModel.programs.push(program);
        
        for (let i = 0; i < document.lineCount; ++i) {
            const line = document.lineAt(i);
            
            if(line.text.toLowerCase().trim().indexOf('/*') == 0)
                isUnderComment = true;

            if(line.text.toLowerCase().indexOf('*/') > -1)
                isUnderComment = false;

            if(isUnderComment)
                continue;

            if(removeComments(line.text).trim().length <= 0)
                continue;

            if(this.isStatement(line.text)){
                i = this.parseStatement(document,line);
            }else if(this.isInclude(line.text)){
                this.readInclude(document,line);
                this.ablModel.cleanMethod();
            }
        }

        this.ablModel.updatePrograms(program); 
    }

    parseStatement(document,line,includeLine = 0):number{
        let text:string = '';
        let ablStatement:AblStatement;

        if (typeof line !== "string"){
            text = line.text;
            ablStatement = this.getFullStatement(document,line);
        }else{ 
            text = line;
            ablStatement = new AblStatement(text,includeLine,0);
        }

        text = text.replace(/\t/gm,' ');
        text = text.replace(/\n/gm,' \n');
        text = text.replace(/\r/gm,' \r');

        if(text.toLowerCase().indexOf(' run ') > -1){
            return this.parseCallProcedure(document,ablStatement);
        }

        if(text.toLowerCase().indexOf(' parameter ') > -1
        || text.toLowerCase().indexOf(' param ') > -1)  {
            return this.parseParameter(document,ablStatement);
        }

        if(text.toLowerCase().indexOf(' temp-table ') > -1){
            //console.log("regex",text.toLowerCase().match(/\s\btemp-table\b\s/gm).length);
            return this.parseTempTable(document,ablStatement);
        }
        
        if(text.toLowerCase().indexOf('procedure ') > -1
        || text.toLowerCase().indexOf('function ') > -1){
            return this.parseMethod(document,ablStatement);
        }        

        if(text.toLowerCase().indexOf('end procedure') > -1
        || text.toLowerCase().indexOf('end function') > -1){
            this.ablModel.setMethodFinalLine(document,ablStatement.lineInit);
        }
        
        if(text.toLowerCase().indexOf(' buffer ') > -1){
            return this.parseBuffer(document,ablStatement);
        }

        if(text.toLowerCase().indexOf(' variable ') > -1
        || text.toLowerCase().indexOf(' var ') > -1){
            return this.parseVariable(document,ablStatement);
        }

        if(text.toLowerCase().indexOf(' dataset ') > -1){
            return this.parseDataset(document,ablStatement);
        }

        this.functions.forEach(element => {
            if(text.includes(element.toString()))
                return this.parseCallProcedure(document,ablStatement);
        });

        if (typeof line !== "string"){
            return line.lineNumber;   
        }     
        
        return 0;
    }

    parseBuffer(document,ablStatement:AblStatement){

        let buffer:BufferTable = new BufferTable();

        let lineComponents:string[] = ablStatement.statement.trim().split(' ');
        buffer.name = lineComponents[2];
        buffer.bufferTable = lineComponents[4].replace(".","");

        buffer.definition = new vscode.Position(ablStatement.lineInit,1);

        this.ablModel.addBufferTable(document,buffer);

        return ablStatement.lineEnd;
    }

    parseTempTable(document,ablStatement:AblStatement){
        
        let fields:string[] = ablStatement.statement.split(' field ');
        let indexes:string[] = ablStatement.statement.split(' index ');
        let table:Table = new Table();

        table.isTempTable = true;        

        for (let i = 0; i < fields.length; ++i) {
            let lineComponents:string[] = fields[i].trim().split(' ');

            if(i == 0){ //temp-table definition
                //get temp table name
                table.name = lineComponents[2];
                if(lineComponents[3] == "like")
                    table.tempLike = lineComponents[4].replace(".","");
                if(lineComponents[4] == "like")
                    table.tempLike = lineComponents[5].replace(".","");
            }else{ //fields definition
                let tableField = new TableField();
                tableField.fieldName = lineComponents[0];
                tableField.fieldType = lineComponents[2];
                if(lineComponents[1] == "like")
                    tableField.isLike = true;
                table.fields.push(tableField);
            }
        }

        if(indexes.length > 1){
            for(let i = 1; i < indexes.length; ++i) {
                let indexComponents:string[] = indexes[i].trim().split(' ');

                let index = new TableIndex();
                index.name = indexComponents[0];

                if(indexComponents.length > 2){
                    let indexFields : IndexField[] = new Array();
                    for(let j = 1; j < indexComponents.length; ++j) {
                        if(indexComponents[j] == "is"
                        || indexComponents[j] == "primary"
                        || indexComponents[j].trim() == ""){
                            index.isPrimary = true;
                            continue;
                        }

                        let indexField:IndexField = new IndexField();
                        indexField.field = indexComponents[j].replace(".","");
                        indexField.descending = false;
                        indexFields.push(indexField);
                    }
                    index.indexFields = indexFields;
                }
                
                table.indexes.push(index);
            }
        }

        table.definition = new vscode.Position(ablStatement.lineInit,1);

        this.ablModel.addTempTable(document,table);

        return ablStatement.lineEnd;
    }

    parseMethod(document,ablStatement:AblStatement):number{
        let lineComponents:string[] = ablStatement.statement.trim().split(' ');
        let method:Method = new Method();
        method.name = lineComponents[1].replace(':','');
        
        if(lineComponents[0] === 'function'){
            method.isFunction = true;
            this.functions.push(method.name);
        }
        
        method.definition = new vscode.Position(ablStatement.lineInit,1);
        method.init = ablStatement.lineInit;
        method.end  = ablStatement.lineEnd;
        this.ablModel.addMethod(document,method);

        return ablStatement.lineEnd;
    }

    parseParameter(document,ablStatement:AblStatement){
        let lineComponents:string[] = ablStatement.statement.trim().split(' ');
        let parameter:Variable = new Variable();

        parameter.isParameter = true;

        if(lineComponents[3].trim() === "table")
            parameter.isTable = true;

        parameter.name = parameter.isTable ? lineComponents[5].replace('.','') : lineComponents[3];
        if(parameter.name.search(".") > 0)
            parameter.name = parameter.name.substr(parameter.name.lastIndexOf("."));
        parameter.fieldType = parameter.isTable ? lineComponents[3] : lineComponents[5];
        parameter.direction = lineComponents[1];
        parameter.additionalInformation = lineComponents[1];
        parameter.definition = new vscode.Position(ablStatement.lineInit,1);
        this.ablModel.addVariable(document,parameter);
        return ablStatement.lineEnd;
    }

    parseCallProcedure(document,ablStatement:AblStatement):number{
        let lineComponents:string[] = ablStatement.statement.trim().split(' ');
        let position:number = 0;

        if(lineComponents[position] != "run"){
            position = position + 1;
        }

        let call:CallProcedure = new CallProcedure();
        
        call.name = lineComponents[position] + " " + lineComponents[position + 1];
        call.definition = new vscode.Position(ablStatement.lineInit,1);
        this.ablModel.addCallProcedure(document,call);

        return ablStatement.lineEnd;
    }

    parseCallFunction(document,ablStatement:AblStatement):number{
        let lineComponents:string[] = ablStatement.statement.trim().split(' ');

        return ablStatement.lineEnd;
    }

    parseVariable(document,ablStatement:AblStatement):number{

        let lineComponents:string[] = ablStatement.statement.trim().split(' ');
        let variable:Variable = new Variable();
        variable.name = lineComponents[2];
        variable.fieldType = lineComponents[4];
        variable.definition = new vscode.Position(ablStatement.lineInit,1);
        this.ablModel.addVariable(document,variable);
        return ablStatement.lineEnd;
    }

    parseDataset(document,ablStatement:AblStatement):number{
        let lineComponents:string[] = ablStatement.statement.trim().split(' ');
        let dataset:Variable = new Variable();
        dataset.name = lineComponents[2];
        dataset.fieldType = "dataset";
        dataset.definition = new vscode.Position(ablStatement.lineInit,1);
        this.ablModel.addVariable(document,dataset);
        return ablStatement.lineEnd;
    }
    /**
     * Utility method to find paths of active documents which takes care
     * of relative names if there are multiple documents of the same name
     *
     * @param {string} docPath Absolute path
     * @returns relative path to show
     * @memberof DocumentManagerClass
     */
    documentDisplayPath(docPath: string) {
        return this.paths[relativePath(docPath)];
    }

    /**
     * Utility method to re-parse a new document.
     *
     * @param {vscode.TextDocument} document
     * @memberof DocumentManagerClass
     */
    resetDocument(document: vscode.TextDocument) {
        this.clearDocumentInternal(document);
        //this.parseDocument(document);
    }

    /**
     * Removes the document from the list of indexed documents.
     *
     * @param {TextDocument} document
     *@memberof DocumentManagerClass
     */
    clearDocument(document: vscode.TextDocument) {
        this.clearDocumentInternal(document);
    }

    /**
     * Internal function that clears a document
     *
     * @private
     * @param {vscode.TextDocument} document The document to clear
     * @memberof DocumentManagerClass
     */
    private clearDocumentInternal(document: vscode.TextDocument) {
        let filename = relativePath(document.fileName);
        let basename = path.basename(filename);
        delete this.paths[filename];
        if (!this.files[basename]) {
            return;
        }
        if (this.files[basename].length === 1) {
            delete this.files[basename];
        } else {
            this.files[basename].splice(this.files[basename].indexOf(filename), 1);
            if (this.files[basename].length === 1) {
                this.paths[this.files[basename][0]] = basename;
            } else {
                for (let i = 0; i < this.files[basename].length; ++i) {
                    this.paths[this.files[basename][i]] = this.files[basename][i];
                }
            }
        }
    }

    private isStatement = function(text:string):boolean{

        if(text.toLowerCase().indexOf('def ') > -1
        || text.toLowerCase().indexOf('define ') > -1
        || text.toLowerCase().indexOf('procedure ') > -1
        || text.toLowerCase().indexOf(' procedure.') > -1
        || text.toLowerCase().indexOf('function ') > -1
        || text.toLowerCase().indexOf(' function.') > -1
        || text.toLowerCase().indexOf(' param ') > -1
        || text.toLowerCase().indexOf(' parameter ') > -1
        || text.toLowerCase().indexOf(' run ') > -1)
            return true;

        if(text.toLowerCase().indexOf('(') > -1){
            this.functions.forEach(element => {
                if(text.includes(element))
                    return true;
            });
        }
        
        return false;
    }

    private isInclude(text:string){
        if(text.toLowerCase().indexOf('{') > -1
        && text.toLowerCase().indexOf('}') > -1
        && text.toLowerCase().indexOf('.i') > -1)
            return true;
        return false;
    }

    private getFullStatement(document: vscode.TextDocument,line:vscode.TextLine):AblStatement{
        let statementLineNumber = line.lineNumber + 1;
        let fullStatement = line.text.replace(/\s\s+/g, ' ');
        let finalStatementLineNumber = 0;
        let currentLine;

        //single line statement
        if(line.text.trim().endsWith(".") || line.text.trim().endsWith(":")){
            return new AblStatement(fullStatement,statementLineNumber,line.lineNumber);
        }

        for (let i = statementLineNumber; i < document.lineCount; ++i) {
            fullStatement = fullStatement.concat(document.lineAt(i).text);

            currentLine = document.lineAt(i).text.trim();
            if (currentLine.endsWith(".") || currentLine.endsWith(":")){
                finalStatementLineNumber = i;
                break;
            }
        }

        //replace double withespaces and tab's for only withespace
        fullStatement = fullStatement.replace(/\s\s+/g, ' ');

        return new AblStatement(fullStatement,line.lineNumber,finalStatementLineNumber);
    }

    private async readInclude(document:vscode.TextDocument,line){

        let include:Program;
        let includeTag:string = '';
        let fileName:String;
        let filePath:string;

        if(line instanceof String
        || typeof(line) == 'string'){
            fileName = line;
        }else{
            fileName = line.text;
        }

        fileName = fileName.replace('{','').replace('}','');
        fileName = fileName.substring(0,fileName.lastIndexOf('.i') + 2);
        includeTag = fileName.toString();
        fileName = vscode.workspace.workspaceFolders[0].uri.fsPath + '/' + fileName.trim();
        fileName = fileName.replace(new RegExp('/', 'g'),"\\");

        include = new Program(fileName);

        if (this.ablModel.hasProgram(fileName)) {
            this.ablModel.addInclude(document,fileName.toString(),includeTag);
            return;
        }

        this.ablModel.programs.push(include);

        filePath = fileName.toString();
        filePath = filePath.trim();

        fs.exists(filePath, (exist) => {
            if (exist && fs.lstatSync(filePath).isFile()){
                this.processInclude(document,fileName,includeTag,filePath);
            }
        });
    }

    public processInclude(document:vscode.TextDocument,fileName:String,includeTag:string,filePath:string){
        let lineNumber:number = 0;
        let statementLine:number = 0;
        let listOfInclude:string = '';
        let concat:boolean = false;
        let fullStatement:string;

        this.ablModel.addInclude(document,fileName.toString(),includeTag);

        var lineReader = require('readline').createInterface({
            input: fs.createReadStream(filePath)
        });
          
        lineReader.on('line', (includeLine) => {
            let lineText:string = includeLine.replace(/\s\s+/g, ' ');

            let isUnderComment:Boolean = false;

            lineNumber++;

            if(!concat){
                statementLine++;
                concat = this.isStatement(lineText.toString());
                if(!concat
                && this.isInclude(lineText)){
                    this.readInclude(document,lineText);
                    this.ablModel.cleanMethod();
                }
                fullStatement = lineText;
            }else{
                fullStatement = fullStatement.concat(lineText);
            }

            if(lineText.indexOf('/*') > -1)
                isUnderComment = true;

            if(lineText.indexOf('*/') > -1)
                isUnderComment = false;

            if (   concat 
                && (lineText.trim().endsWith(".") || lineText.trim().endsWith(":"))
                && !isUnderComment){
                concat = false;
                statementLine = lineNumber;
                this.parseStatement(fileName,fullStatement,statementLine);   
            }
        });
    }

    public getMap(document:vscode.TextDocument): Object {

        return {
            methods: toGPSMethod(this.ablModel.getMap(document,false)),
            variables: toGPSVariables(this.ablModel.getAblVariables(document,true)),
            tempTables: toGPSTempTables(this.ablModel.getAblTableObjects(document,true,true)),
            includes: this.toGPSIncludes(this.ablModel.getProgram(document)),
            external: undefined
        };
    }
    
    public toGPSIncludes(program:Program):ABLInclude[]{
        let gpsIncludes:ABLInclude[] = new Array();
    
        program.includes.forEach(include => {
            let gpsInclude:ABLInclude = new ABLInclude();
            gpsInclude.name = include.tag;
            if(gpsInclude.name == '')
                gpsInclude.name = include.program.program.toString();
            gpsInclude.fsPath = include.path;       
            gpsInclude.map = {
                variables: toGPSVariables(this.ablModel.getAblVariables(include.program.program,true)),
                tempTables: toGPSTempTables(this.ablModel.getAblTableObjects(include.program.program,true,true))
            };
            gpsIncludes.push(gpsInclude);
        });
    
        return gpsIncludes;
    
    }
}
