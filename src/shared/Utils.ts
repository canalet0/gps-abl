/**
 * COPYRIGHT 2017 Atishay Jain<contact@atishay.me>
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

'use strict';
import * as vscode from 'vscode';
import { Method, Variable, Table, Program } from '../models/ablObject';
import { ABLMethod, ABLParameter, ABL_PARAM_DIRECTION, ABL_ASLIKE, ABLVariable, ABLTempTable, ABLInclude } from '../models/gpsObjects';

/**
 * Checks if the file is marked for exclusion by the user settings
 *
 * @export
 * @param {string} file
 * @returns {boolean}
 */
export function shouldExcludeFile(file: string): boolean {
    return false;
    /*var filename = path.basename(file);
    if (Settings.buildInFilesToExclude.indexOf(filename) !== -1) {
        return true;
    }
    if (Settings.buildInRegexToExclude.find((regex) => Array.isArray(file.match(regex))) !== undefined) {
        return true;
    }
    return minimatch(this.relativePath(file), Settings.excludeFiles);*/
}

/**
 * Converts document path to relative path
 *
 * @export
 * @param {string} filePath
 * @returns
 */
export function relativePath(filePath: string) {
    return vscode.workspace.asRelativePath(filePath);
}

/**
 * Finds active documents by cycling them.
 *
 * @returns
 */
export function findActiveDocsHack() {
    // Based on https://github.com/eamodio/vscode-restore-editors/blob/master/src/documentManager.ts#L57
    return new Promise((resolve, reject) => {
        let active = vscode.window.activeTextEditor as any;
        let editor = active;
        const openEditors: any[] = [];
        function handleNextEditor() {
            if (editor !== undefined) {
                // If we didn't start with a valid editor, set one once we find it
                if (active === undefined) {
                    active = editor;
                }

                openEditors.push(editor);
            }
            // window.onDidChangeActiveTextEditor should work here but I don't know why it doesn't
            setTimeout(() => {
                editor = vscode.window.activeTextEditor;
                if (editor !== undefined && openEditors.some(_ => _._id === editor._id)) return resolve();
                if ((active === undefined && editor === undefined) || editor._id !== active._id) return handleNextEditor();
                resolve();
            }, 500);
            vscode.commands.executeCommand('workbench.action.nextEditor')
        }
        handleNextEditor();
    });
}
export function toObjectName(name:String):string{
    let returnString:String = ""; 
    let toUpper:boolean = false;

    for(var i = 0; i < name.length; i++){

        let character = toUpper ? name.charAt(i).toUpperCase() : name.charAt(i).toLowerCase();

        toUpper = false;

        if(character === "-")
            toUpper = true;

        returnString += character;

    }

    return returnString.replace(new RegExp('-', 'g'),"");

}

export function padRight(text:string,size:number,textToPad?:string):string{
    text = text + "";
    

    let s = text;
    if(textToPad == null)
        while (s.length < size) s = s + " ";
    else while (s.length < size) s = s + textToPad;

    return s;
}

export function padLeft(text:string,size:number,textToPad?:string):string{
    text = text + "";
    

    let s = text;
    if(textToPad == null)
        while (s.length < size) s = " " + s;
    else while (s.length < size) s = textToPad + s;

    return s;
}

export function toGPSMethod(methods:Method[]):ABLMethod[]{
    let gpsMethods:ABLMethod[] = new Array();

    methods.forEach(method => {
        let gpsMethod:ABLMethod = new ABLMethod();
        gpsMethod.name = method.name.toString();
        gpsMethod.lineAt = method.init.valueOf();
        gpsMethod.lineEnd = method.end.valueOf();
        gpsMethod.params = new Array();

        method.parameters.forEach(parameter => {

            let gpsParameter:ABLParameter = new ABLParameter();

            gpsParameter.name = parameter.name.toString();
            gpsParameter.line = parameter.definition.line;
            gpsParameter.dataType = parameter.fieldType.toString();
            gpsParameter.asLike = ABL_ASLIKE.AS;

            if(parameter.isTable)
                gpsParameter.dataType = "temp-table";

            if(parameter.direction == 'input')
                gpsParameter.direction = ABL_PARAM_DIRECTION.IN;
            else if(parameter.direction == 'input-output')
                gpsParameter.direction = ABL_PARAM_DIRECTION.INOUT;
            else gpsParameter.direction = ABL_PARAM_DIRECTION.OUT;

            gpsMethod.params.push(gpsParameter);
        });

        gpsMethods.push(gpsMethod);
    });

    return gpsMethods;
}

export function toGPSVariables(variables:Variable[]):ABLVariable[]{
    let gpsVariables:ABLVariable[] = new Array();
    variables.forEach(variable => {
        let gpsVariable = new ABLVariable();
        gpsVariable.name = variable.name.toString();
        gpsVariable.line = variable.definition.line;
        gpsVariable.dataType = variable.fieldType.toString();
        gpsVariable.asLike = ABL_ASLIKE.AS;
        gpsVariables.push(gpsVariable);
    });
    return gpsVariables;
}

export function toGPSTempTables(tempTables:Table[]):ABLTempTable[]{
    let gpsTempTables:ABLTempTable[] = new Array();

    tempTables.forEach(tempTable => {
        let gpsTempTable:ABLTempTable = new ABLTempTable();
        gpsTempTable.fields = new Array();
        gpsTempTable.line = tempTable.definition.line;
        gpsTempTable.label = tempTable.name.toString();

        tempTable.fields.forEach(field => {
            let gpsVariable = new ABLVariable();
            gpsVariable.name = field.fieldName.toString();
            gpsVariable.line = tempTable.definition.line;
            gpsVariable.dataType = field.fieldType.toString();
            gpsVariable.asLike = ABL_ASLIKE.AS;
            gpsTempTable.fields.push(gpsVariable);
        });
        gpsTempTables.push(gpsTempTable);
    });


    return gpsTempTables;
}

export function removeComments(text:string):string{
    let result:string = text;
    if(result.lastIndexOf('*\/') > 1)
        result = result.replace(/\/\*[\s\S]*?\*\/|\/\/.*/,'');

    return result;
}